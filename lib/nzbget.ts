import { Client } from '@jc21/nzbget-jsonrpc-api'
import { URL } from 'url'

declare global {
  // allow global `var` declarations
  // eslint-disable-next-line no-var
  var nzbget: Client | undefined
}

export const nzbget = global.nzbget ?? new Client(new URL(process.env.NZBGET_URL))

if (process.env.NODE_ENV !== 'production') global.nzbget = nzbget
