import { NewznabItem } from '@prisma/client'
import type { NextApiRequest, NextApiResponse } from 'next'

import { nzbget } from '../../../lib/nzbget'
import { NZBHydra } from '../../../lib/nzbhydra'
import { prisma } from '../../../lib/prisma'
import { getNewnabItemStatus, HTTPMethod, promiseSerial } from '../../../lib/utils'

interface QueryParams {
  q: string
  stashId?: string
}

const isQuery = (obj: any): obj is QueryParams => obj.q !== undefined && !Array.isArray(obj.q)

const nzbhydra = new NZBHydra(process.env.NZBHYDRA_API_KEY, process.env.NZBHYDRA_ENDPOINT)

const handler = async ({ method, query }: NextApiRequest, res: NextApiResponse<NewznabItem[]>): Promise<void> => {
  switch (method) {
    case HTTPMethod.GET: {
      if (!isQuery(query)) {
        res.status(400).end('Invalid params')
        return
      }

      const { q, stashId } = query

      const nzbhydraItems = await nzbhydra.search(q)

      const [activeItems, downloadedItems] = await Promise.all([nzbget.listgroups(), nzbget.history(true)])
      const nzbgetItems = [...activeItems, ...downloadedItems]

      await promiseSerial(
        nzbhydraItems.map(
          ({ guid: id, url, ...item }) =>
            () =>
              prisma.newznabItem.upsert({
                where: { id },
                create: {
                  id,
                  url,
                  ...item,
                  queryName: q,
                  stashId,
                  status: getNewnabItemStatus(nzbgetItems.find(({ URL }) => URL === url)?.Status)
                },
                update: {
                  url,
                  queryName: q,
                  stashId,
                  status: getNewnabItemStatus(nzbgetItems.find(({ URL }) => URL === url)?.Status)
                }
              })
        )
      )
      const items = await prisma.newznabItem.findMany()
      res.status(200).json(items)
      break
    }

    default: {
      res.setHeader('Allow', [HTTPMethod.GET])
      res.status(405).end(`Method ${method ?? ''} not allowed`)
    }
  }
}

export default handler
