import '../styles/globals.css'

import { AppProps } from 'next/app'
import { DefaultSeo } from 'next-seo'

const App = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <>
      <DefaultSeo titleTemplate="%s | Boobielicious&reg;" title="Home of your melon queens" />
      <Component {...pageProps} />
    </>
  )
}

export default App
