import { NextPage } from 'next'

const HomePage: NextPage = () => (
  <main className="flex justify-center items-center h-screen">
    <h1 className="font-bold text-5xl text-neutral-900 dark:text-neutral-50 tracking-tighter select-none">
      Boobielicious&reg;
    </h1>
  </main>
)

export default HomePage
